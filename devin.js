// Renvoyer un entier aléatoire entre 1 et 100
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}

function devin() {
  
  let count = 0
  let resultat = getRandomInt(1, 101)
  let choix = -1
  while (choix != resultat) {
    try {
      choix = prompt("Saisir un entier entre 1 et 100 :");
      count++
      if (!Number.isInteger(Number(choix))){
        throw new Error("Un entier svp !");
      }
    }
    catch(e) {
      alert(e.message);
      continue;
    }
    if (choix < resultat) {
      window.alert("Plus grand !");
    } else {
      window.alert("Plus petit !");
    } 
  } 
  
  window.alert(`Gagné en ${count} coups !`)
}
