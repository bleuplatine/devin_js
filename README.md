// Renvoyer un entier aléatoire entre 1 et 100
function getRandomInt(min, max) {
min = Math.ceil(min);
max = Math.floor(max);
return Math.floor(Math.random() * (max - min)) + min;
}
let valeur = getRandomInt(1, 101);
console.log(valeur)
// Demander la saisie d'une valeur
let saisie = Number(-1)
while (saisie < 0 || saisie > 10) {
saisie = Number(prompt("Saisir un chiffre entre 1 et 10 :"));
}
console.log("OK !")
// Afficher un message d'alerte
window.alert("Bravo !");